### Analysis of behaviour of Bitbucket when squashing commits of a PR into another branch

In this repository look at the file `test.txt`.

This has been edited in the `TestBranch2` branch, by _Joe Bloggs_. A [PR](https://bitbucket.org/jameswebster_rhipe/squashcommittest/pull-requests/2) merged this change from `TestBranch2` into `master`, using _Merge with squash_ the merge was performed by _James Webster_. A consequence of squashing the merge the individual commits in `TestBranch2` do not appear in `master`, just a single merge commit which is as expected. Furthermore, looking at the `git blame` output the line-by-line changes that were made in `TestBranch2` are no longer attributed to _Joe Bloggs_, but are instead attributed to _James Webster_ who performed the merge.

In this repo, the `TestBranch2` hasn't been deleted from `origin` so it should be possible to see that the branch appears to be dangling.

Therefore some potentially useful information is lost in the process of _Merge with squash_, depending on your point-of-view.

One strategy that has been considered is performing squash merge from feature/story branches to release branches (i.e. `NPP-9999` into `R-<ReleaseName>`) but merging with commit history from the release branch into `master`. Therefore the project history in `master` will retain multiple commits for each release as it is merged into `master`.